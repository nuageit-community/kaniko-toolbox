# Base image for cosign binary
FROM gcr.io/projectsigstore/cosign:v2.2.2 as cosign-bin
# Base image for kaniko binary
FROM gcr.io/kaniko-project/executor:v1.19.2-debug as kaniko
# Base image for trivy binary
FROM ghcr.io/aquasecurity/trivy:0.48.3 as trivy

# Final base image
FROM amazon/aws-cli:2.15.9
# Copy cosign from cosign-bin stage
COPY --from=cosign-bin ["/ko-app/cosign", "/usr/local/bin/cosign"]
# Copy kaniko from kaniko stage
COPY --from=kaniko [ "/kaniko/executor", "/opt/kaniko/executor" ]
COPY --from=kaniko [ "/kaniko/warmer", "/opt/kaniko/warmer" ]
COPY --from=kaniko [ "/kaniko/docker-credential-ecr-login", "/opt/kaniko/docker-credential-ecr-login" ]
COPY --from=kaniko [ "/kaniko/.docker", "/opt/kaniko/.docker" ]
ENV PATH $PATH:/usr/local/bin:/opt/kaniko
ENV DOCKER_CONFIG /opt/kaniko/.docker/
ENV SSL_CERT_DIR /opt/kaniko/ssl/certs
# Copy trivy from trivy stage
COPY --from=trivy ["/usr/local/bin/trivy", "/usr/local/bin/trivy"]
# Set default command
ENTRYPOINT [ "/bin/bash" ]
