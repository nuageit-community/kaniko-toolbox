<!-- BEGIN_TF_DOCS -->
<div align="center">

<a name="readme-top"></a>

<img alt="gif-header" src="https://gitlab.com/nuageit-community/images-toolbox/-/raw/main/.gitlab/assets/gif-header.gif" width="225"/>

<h2 align="center">Kaniko Toolbox</h2>

[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)]()
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)]()

---

<img alt="gif-about" src="https://gitlab.com/nuageit-community/images-toolbox/-/raw/main/.gitlab/assets/gif-about.gif" width="225"/>

<p>Docker image containing the Kaniko CLI configuration to we use in the pipeline</p>

<p>
  <a href="#-getting-started-">Getting Started</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-explore-">Explore</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-versioning-">Versioning</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-troubleshooting-">Troubleshooting</a>
</p>

</div>

---

## ➤ Getting Started

### Setup

To configure your system for the development of this project, follow the steps below:

- Clone the git repository to your local environment.
- Install [asdf](https://asdf-vm.com/) to manage runtime dependencies.
- Install runtime dependencies.

```bash
cut -d' ' -f1 .tool-versions | xargs -I{} sh -c 'asdf plugin add "$1"' -- {} && asdf install
```

- Follow the instructions after installing the tools in `.tool-versions` with `asdf`.
- Run task from the root of the repository to see available commands. We use task in place of make for this project. See [Taskfile.yml](Taskfile.yml) for more information.

<p align="right">(<a href="#cluster-management">back to top</a>)</p>

## ➤ Versioning

To check the change history, please access the [**CHANGELOG.md**](CHANGELOG.md) file.

<p align="right">(<a href="#cluster-management">back to top</a>)</p>

## ➤ Troubleshooting

If you have any problems, please contact **DevOps Team**.

<p align="right">(<a href="#cluster-management">back to top</a>)</p>

## ➤ Show your support

<div align="center">

Give me a ⭐️ if this project helped you!

<img alt="gif-footer" src="https://gitlab.com/nuageit-community/images-toolbox/-/raw/main/.gitlab/assets/gif-footer.gif" width="225"/>

Made with 💜 by **DevOps Team** :wave: inspired on [readme-md-generator](https://github.com/kefranabg/readme-md-generator)

</div>

<p align="right">(<a href="#cluster-management">back to top</a>)</p>
