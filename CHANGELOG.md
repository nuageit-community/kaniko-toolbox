# Semantic Versioning Changelog

## [1.2.5](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.2.4...1.2.5) (2024-01-15)


### :bug: Fixes

* force kaniko version ([43b681f](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/43b681f772ad502cd952941f986687745337707a))

## [1.2.4](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.2.3...1.2.4) (2024-01-15)


### :bug: Fixes

* copy credential helpers ([37b4ea2](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/37b4ea2d7eabeb3b722310d37449fda6128a18f2))

## [1.2.3](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.2.2...1.2.3) (2024-01-15)


### :bug: Fixes

* copy kaniko files ([42edfad](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/42edfadf92f21dfa8612ef3aab8b5a0d03d04678))

## [1.2.2](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.2.1...1.2.2) (2024-01-15)


### :bug: Fixes

* kaniko image ([9e14d83](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/9e14d8398012fa877c2a118225120a729bb38339))

## [1.2.1](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.2.0...1.2.1) (2024-01-13)


### :bug: Fixes

* copy files ([9e3396f](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/9e3396fb99957696d4a6acc49a2c53c05c818015))

## [1.2.0](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.1.1...1.2.0) (2024-01-13)


### :sparkles: News

* **dockerfile:** add aws cli ([c41190b](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/c41190b9919a28bbe27488a8e37a97acb07d7b55))

## [1.1.1](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.1.0...1.1.1) (2024-01-13)


### :bug: Fixes

* cosign install ([1b4ce49](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/1b4ce49984bf324af78c235d366db7dd93d306aa))

## [1.1.0](https://gitlab.com/nuageit-community/kaniko-toolbox/compare/1.0.0...1.1.0) (2024-01-13)


### :sparkles: News

* add build cosign ([6cba42b](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/6cba42b709b655e6d4fb0723dc98d64fe81daad5))

## 1.0.0 (2024-01-12)


### :zap: Refactoring

* update file name ([ddf5dcf](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/ddf5dcf3c619a9b39565f93b6707e9ed435c75d2))


### :repeat: CI

* add include job ([5286464](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/5286464cd3de6643f464fd089090715f832c247a))
* change include ([d919dc4](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/d919dc45438796205346c623c5dec56c4f6fe5d0))
* change ref name ([bfb14ac](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/bfb14ac42f9a4bc8844e78f49a12c622647b3e85))


### :sparkles: News

* add commands ([402d5bb](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/402d5bb31a83157733a68429d67272d3b3aec864))
* add install cosign ([c6fe297](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/c6fe29727c11e5b25f3990be1cc753e1e0e37f53))
* add install trivy ([ce6a92b](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/ce6a92b06a246fdb2f1cf20d57e479be5a451801))
* add quotes ([4cf38d2](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/4cf38d2df101df8c352b5a1c8d50da8a8bff269d))
* kaniko setup ([9555026](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/9555026afd6b8616ea7bd796a22e482fbb970ec5))


### :memo: Docs

* add button ([2ed28bb](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/2ed28bb742bf3f7d87935cd4f169d43be3bf797d))
* change readme ([8282ec2](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/8282ec292ab67c0d0922f46c4f7e8ec8e593a566))
* change script ([81c1f4d](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/81c1f4d7258b7c87f84bc466cc687009416ce2e6))
* pretty readme ([b294600](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/b294600902dbc3781348cdaaafa4004b9bc0a546))
* pretty readme [skip ci] ([53ecf6c](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/53ecf6c479e8b2a811fd633c1546e25614509f66))


### :bug: Fixes

* cosign file ([c8ba899](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/c8ba899c18e8cbbcf9142fbe9594a07576cdabdd))
* hooks setup ([d7d64da](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/d7d64daf11c05f264974ea052b9677ae2a6efdf8))
* identation yaml ([dec8ae8](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/dec8ae88d0d9f0b285a49f76538a8ec4dd0ce7c6))
* image dive version ([2f70768](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/2f70768dae0dc6bf886c07b2b9ed990b8fb94d0a))
* image install tools ([03f974b](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/03f974b08d54346d6c5b63d3b34235c05ef23cff))
* organize project setup ([83b2a6a](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/83b2a6abc99dde9752a8bf2f774f4c5a53045efa))
* pre-commit and makefile ([046118a](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/046118ab483e77df75d88cb00e19c0ff3358236f))
* remove dive ([bd05c52](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/bd05c52203f012960d94d7af3e12a09ee939d9ca))
* remove empty line ([c19375c](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/c19375c04e9c707d7fc7775e80473b6cab60bd65))
* rm file ([efcb08c](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/efcb08cd037d7b2cad8979ca88b2a248b839f3cc))
* setup docs and taskfile ([35a8888](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/35a888890d8deee1dd467ed242d72c54e93c7176))
* var version ([2b5b806](https://gitlab.com/nuageit-community/kaniko-toolbox/commit/2b5b806373851036c80f25e8acca945f2f954474))
